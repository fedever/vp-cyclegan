# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function, unicode_literals
import tensorflow_datasets as tfds
import os
import time
import matplotlib.pyplot as plt
from IPython.display import clear_output
import tensorflow as tf
import tensorflow.contrib.eager as tfe
import tqdm
import numpy as np
tf.reset_default_graph()
tf.enable_eager_execution()
AUTOTUNE = tf.data.experimental.AUTOTUNE

dataset, metadata = tfds.load('cycle_gan/vangogh2photo',
                              with_info=True, as_supervised=True)

train_vgg, train_photo = dataset['trainA'], dataset['trainB']
test_vgg, test_photo = dataset['testA'], dataset['testB']

IMG_HEIGHT, IMG_WIDTH = 256, 256
BUFFER_SIZE = 1000
BATCH_SIZE = 10
EPOCHS = 200
LEARNING_RATE = 0.0001

def random_crop(image):
    cropped_image = tf.image.random_crop(
    image, size=[IMG_HEIGHT, IMG_WIDTH, 3])

    return cropped_image

# normalizing the images to [0, 1]
def normalize(image):
    image = tf.cast(image, tf.float32)
    image = image / 255
    return image

def random_jitter(image):
    # resizing to 286 x 286 x 3
    image = tf.image.resize(image, [286, 286],
                          method=tf.image.ResizeMethod.NEAREST_NEIGHBOR)

    # randomly cropping to 256 x 256 x 3
    image = random_crop(image)

    # random mirroring
    image = tf.image.random_flip_left_right(image)

    return image

def preprocess_image(image, label):
    image = random_jitter(image)
    image = normalize(image)
    return image

# Load dataset
train_vgg = train_vgg.map(
    preprocess_image, num_parallel_calls=AUTOTUNE).cache().shuffle(
    BUFFER_SIZE).batch(BATCH_SIZE)

train_photo = train_photo.map(
    preprocess_image, num_parallel_calls=AUTOTUNE).cache().shuffle(
    BUFFER_SIZE).batch(BATCH_SIZE)

test_vgg = test_vgg.map(
    preprocess_image, num_parallel_calls=AUTOTUNE).cache().shuffle(
    BUFFER_SIZE).batch(BATCH_SIZE)

test_photo = test_photo.map(
    preprocess_image, num_parallel_calls=AUTOTUNE).cache().shuffle(
    BUFFER_SIZE).batch(BATCH_SIZE)

# Transforming into np arrays
train_vgg = np.asarray([example for example in tfds.as_numpy(train_vgg)])
train_photo = np.asarray([example for example in tfds.as_numpy(train_photo)])
test_vgg = np.asarray([example for example in tfds.as_numpy(test_vgg)])
test_photo = np.asarray([example for example in tfds.as_numpy(test_photo)])


def residual_block(x, filters, kernel_size, strides):
    """
    Residual block
    """
    conv = tf.layers.conv2d(inputs = x, filters = filters, kernel_size = kernel_size, padding = 'same',strides = strides)
    batch_norm = tf.contrib.layers.instance_norm(conv)
    relu = tf.nn.relu(batch_norm)
        
    conv2 = tf.layers.conv2d(inputs = relu, filters = filters, kernel_size = kernel_size, padding = 'same',strides = strides)
    res = tf.contrib.layers.instance_norm(conv2)

    return res+x

def upsample_block(x, filters, kernel_size, strides):
    up = tf.layers.conv2d_transpose(inputs = x, filters=filters, kernel_size=kernel_size, strides=strides, padding='same', use_bias=False)
    batch_up = tf.contrib.layers.instance_norm(up)
    return tf.nn.relu(batch_up)

def generator(z, name, reuse=None):
    with tf.variable_scope(name,reuse=reuse):        
        # --- Encoder network ----
        # - First Convolutional block -
        conv1 = tf.layers.conv2d(inputs = z, filters = 64, kernel_size = 7, padding = 'same',strides = 1, )
        batch_norm1 = tf.contrib.layers.instance_norm( conv1)
        relu1 = tf.nn.relu(batch_norm1)
        # - Second Convolutional block -
        conv2 = tf.layers.conv2d(inputs = relu1, filters = 128, kernel_size = 3, padding = 'same',strides = 2)
        batch_norm2 = tf.contrib.layers.instance_norm(conv2)
        relu2 = tf.nn.relu(batch_norm2)
        # - Third Convolutional block -
        conv3 = tf.layers.conv2d(inputs = relu2, filters = 256, kernel_size = 3, padding = 'same',strides = 2)
        batch_norm3 = tf.contrib.layers.instance_norm(conv3)
        relu3 = tf.nn.relu(batch_norm3)
        # - Fourth Residual block 
        residual4 = residual_block(relu3, 256, 3, 1)
        # - Fifth Residual block
        residual5 = residual_block(residual4, 256, 3, 1)
        # - Sixth Residual block
        residual6 = residual_block(residual5, 256, 3, 1)
        # - Seventh Residual block
        residual7 = residual_block(residual6, 256, 3, 1)
        # - Eighth Residual block
        residual8 = residual_block(residual7, 256, 3, 1)
        # - Nineth Residual block
        residual9 = residual_block(residual8, 256, 3, 1)

        # --- Decoder network ----
        # - First Upsampling Convolutional Block -
        up1 = upsample_block(residual9, 256, 3, 2) #(bs, 64, 64, 256)
        # - Second Upsampling Convolutional Block -
        up2 = upsample_block(up1, 128, 3, 2)   #(bs, 128, 128, 128)
        # - Third Upsampling Convolutional Block -
        up3 = upsample_block(up2, 64, 3, 1)   #(bs, 128, 128, 64)
        
        # - Fourth Upsampling Convolutional Block -
        up4 = tf.layers.conv2d(inputs = up3, filters = 3, kernel_size = 7, padding = 'same',strides = 1, )
                
        return tf.nn.tanh(up4)

def discriminator(inputs, name, reuse=None):
    with tf.variable_scope(name,reuse=reuse):
        leakyrelu_alpha = 0.2
        # First Convolution Block 
        conv1 = tf.layers.conv2d(inputs = inputs, filters = 64, kernel_size = 4, padding = 'valid',
                            strides = 2)
        leaky_relu = tf.nn.leaky_relu(conv1, alpha = leakyrelu_alpha)
        
        # Second Convolution Block 
        conv2 = tf.layers.conv2d(inputs = leaky_relu, filters = 128, kernel_size = 4, padding = 'valid',
                            strides = 2)
        batch_norm2 = tf.contrib.layers.instance_norm(conv2)
        leaky_relu2 = tf.nn.leaky_relu(batch_norm2, alpha = leakyrelu_alpha)
        
        # Third Convolution Block 
        conv3 = tf.layers.conv2d(inputs = leaky_relu2, filters = 256, kernel_size = 4, padding = 'valid',
                            strides = 2)
        batch_norm3 = tf.contrib.layers.instance_norm(conv3)
        leaky_relu3 = tf.nn.leaky_relu(batch_norm3, alpha = leakyrelu_alpha)
        
        # Fourth Convolution Block 
        conv4 = tf.layers.conv2d(inputs = leaky_relu3, filters = 512, kernel_size = 4, padding = 'valid',
                            strides = 2)
        batch_norm4 = tf.contrib.layers.instance_norm(conv4)
        leaky_relu4 = tf.nn.leaky_relu(batch_norm4, alpha = leakyrelu_alpha)
        
        # Last Convolution Block
        conv5 = tf.layers.conv2d(inputs = leaky_relu4, filters = 1, kernel_size = 4, strides = 1, padding='valid')
        
        return tf.nn.sigmoid(conv5)

def cycle_loss(cycle_image_real, cycle_image_gen):
    return tf.reduce_mean(tf.abs(cycle_image_real - cycle_image_gen))

def generator_loss(prob_fake_is_real):
    return tf.reduce_mean(tf.square(prob_fake_is_real - tf.ones_like(prob_fake_is_real)))

def discriminator_loss(prob_real_is_real, prob_fake_is_real):
    return 0.5*(tf.reduce_mean(tf.square(prob_real_is_real - tf.ones_like(prob_real_is_real)))
                + tf.reduce_mean(tf.square(prob_fake_is_real - tf.zeros_like(prob_fake_is_real))))

# Input placeholders
input_vgg = tf.placeholder(tf.float32,shape=[None,IMG_HEIGHT,IMG_WIDTH,3],name="input_vgg")
input_photo = tf.placeholder(tf.float32,shape=[None,IMG_HEIGHT,IMG_WIDTH,3],name="input_photo")

# Generation of fake images
fake_photo_gen = generator(input_vgg,"gen_photo_from_vgg") # Takes VGG images to generate photos
fake_vgg_gen = generator(input_photo,"gen_vgg_from_photo") # Takes photo images to generate VGG

# Cycling back
cycle_vgg_gen   = generator(fake_photo_gen,"gen_vgg_from_photo", reuse=True) # Takes generated photo and go back to vgg
cycle_photo_gen = generator(fake_vgg_gen,"gen_photo_from_vgg", reuse=True)   # Takes generated vgg and go back to photo

# Discriminator of real inputs
prob_real_vgg_is_real_gen = discriminator(input_vgg,"disc_vgg") # Discriminate vgg from input vgg
prob_real_photo_is_real_gen = discriminator(input_photo,"disc_photo") # Discriminate photo from input photo

# Discriminator of fake inputs
prob_fake_vgg_is_real_gen   = discriminator(fake_vgg_gen,"disc_vgg", reuse=True) # Discriminate vgg from generated vgg
prob_fake_photo_is_real_gen = discriminator(fake_photo_gen,"disc_photo", reuse=True) # Discriminate photo from generated photo

# Discriptive loss 
disc_vgg_loss = discriminator_loss(prob_real_vgg_is_real_gen, prob_fake_vgg_is_real_gen)
disc_photo_loss = discriminator_loss(prob_real_photo_is_real_gen, prob_fake_photo_is_real_gen)

# Generative loss 
gen_vgg_loss = generator_loss(prob_fake_vgg_is_real_gen)
gen_photo_loss = generator_loss(prob_fake_photo_is_real_gen)

# Cycle Loss
cycle_vgg_loss = cycle_loss(input_vgg, cycle_vgg_gen)
cycle_photo_loss = cycle_loss(input_photo, cycle_photo_gen)
          
LAMBDA = 10
total_cycle_loss = cycle_vgg_loss + cycle_photo_loss

# Total gen loss
total_gen_vgg_loss = gen_vgg_loss + 10*total_cycle_loss 
total_gen_photo_loss = gen_photo_loss + 10*total_cycle_loss

# Optimize the loss 

#optimize the discriminative loss
disc_vgg_opt = tf.train.AdamOptimizer(learning_rate=LEARNING_RATE).minimize(disc_vgg_loss, 
                                                                    var_list = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES,"disc_vgg"))
disc_photo_opt = tf.train.AdamOptimizer(learning_rate=LEARNING_RATE).minimize(disc_photo_loss, 
                                                                    var_list = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES,"disc_photo"))
#Optimize the generaive loss 
gen_vgg_opt =  tf.train.AdamOptimizer(learning_rate=LEARNING_RATE).minimize(total_gen_vgg_loss,
                                                                    var_list = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES,"gen_vgg_from_photo"))
gen_photo_opt =  tf.train.AdamOptimizer(learning_rate=LEARNING_RATE).minimize(total_gen_photo_loss,
                                                                    var_list = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES,"gen_photo_from_vgg"))

def generate_images(test_input, pred, cycle, epoch, name):
    fig, (ax1, ax2, ax3) = plt.subplots(1, 3)
    plt.figure(figsize=(12, 12)) 
    
    ax1.imshow(test_input[0])
    ax2.imshow(pred[0])
    ax3.imshow(cycle[0])
    
    plt.show()
    plt.savefig('figure_'+name+'_epoch_'+str(epoch)+'.png')

try:
    with tf.Session(config=config) as sess:
        sess.run(tf.global_variables_initializer())
        # Add ops to save and restore all the variables.
        saver = tf.train.Saver()
        #saver.restore(sess, "/content/drive/My Drive/CycleGAN/best_model2.ckpt")
        #print("Model restored.")
        discriminator_loss_per_epoch = []
        generator_loss_per_epoch = []
        for epoch in range(EPOCHS):
            i=0
            discriminator_loss_per_step = []
            generator_loss_per_step = []
            for image_vgg, image_photo in tqdm.tqdm(zip(train_vgg, train_photo), total = len(list(zip(train_vgg, train_photo)))):
                # Generate the fake images
                fake_vgg, fake_photo = sess.run([fake_vgg_gen, fake_photo_gen],feed_dict={input_vgg:image_photo, 
                                                                                           input_photo:image_vgg})              
                # Update loss 
                _, disc_vgg_loss_update = sess.run([disc_vgg_opt, disc_vgg_loss], 
                                                   feed_dict={input_vgg:image_vgg,
                                                              input_photo:image_photo})

                _, disc_photo_loss_update = sess.run([disc_photo_opt, disc_photo_loss], 
                                                     feed_dict={input_photo:image_photo,
                                                                input_vgg:image_vgg})

                _, gen_vgg_loss_update = sess.run([gen_vgg_opt, total_gen_vgg_loss], 
                                                  feed_dict={input_vgg:image_vgg,
                                                             input_photo:image_photo
                                                            })

                _, gen_photo_loss_update = sess.run([gen_photo_opt, total_gen_photo_loss], 
                                                  feed_dict={input_vgg:image_vgg,
                                                             input_photo:image_photo
                                                            })

                discriminator_loss_per_step.append(disc_vgg_loss_update+disc_photo_loss_update)
                generator_loss_per_step.append(gen_vgg_loss_update+gen_photo_loss_update)

            discriminator_loss_per_epoch.append(np.mean(np.asarray(discriminator_loss_per_step)))
            generator_loss_per_epoch.append(np.mean(np.asarray(generator_loss_per_step)))
            print("\n Epoch: ", epoch )
            print("Discriminator loss ", discriminator_loss_per_epoch[-1])
            print("Generator loss", generator_loss_per_epoch[-1])
            print("\n")

            if epoch % 5 == 0:
            	# Print samples every five epochs
            	# Get a random index
                index_vgg = np.random.choice(test_vgg.shape[0], 1, replace=False) 
                index_photo = np.random.choice(test_photo.shape[0], 1, replace=False) 

                photo = np.expand_dims(test_photo[index_photo][0][0], axis=0)
                vgg = np.expand_dims(test_vgg[index_vgg][0][0], axis=0)

                fake_vgg, fake_photo = sess.run([fake_vgg_gen,fake_photo_gen],feed_dict={input_vgg:photo, 
                                                                                           input_photo:vgg})
                cycle_vgg, cycle_photo = sess.run([cycle_vgg_gen, cycle_photo_gen], feed_dict={input_vgg:fake_vgg,
                                                                                              input_photo:fake_photo})
                print("Photo to VGG")
                generate_images(photo, fake_photo, cycle_vgg, epoch, 'Photo')
                print("VGG to Photo")
                generate_images(vgg, fake_vgg, cycle_photo, epoch, 'VGG')
                
            if epoch % 2 == 0:
            	# Save model every two epochs
                save_path = saver.save(sess, "/content/drive/My Drive/CycleGAN/best_model2.ckpt")
                print("Model saved ")
                
except KeyboardInterrupt:
    save_path = saver.save(sess, "/content/drive/My Drive/CycleGAN/best_model2.ckpt")
    print("Model saved ")